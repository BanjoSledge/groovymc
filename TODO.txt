TODO:
Short-Term: (Before 1.13/1.14 Port Completion)
- Complete Variable Model
- To Fix:
    - CuboidBase: VariableLight
- BakedModel's

Long-Term:
- GroovyMC Model inter compatible with Forge's IModel API
- GroovyMC Model inter compatible with Vanilla ModelBase
- MultiBlocks
- GuiBuilder
- Improve Modules